resource "helm_release" "loki" {
  name             = "loki"
  chart            = "loki-stack"
  repository       = "https://grafana.github.io/loki/charts"
  namespace        = var.monitoring_namespace
  create_namespace = true
  count            = local.monitoring_count

  set {
    name  = "fluent-bit.enabled"
    value = false
  }

  set {
    name  = "promtail.enabled"
    value = true
  }

  set {
    name  = "loki.serviceName"
    value = "loki.${var.monitoring_namespace}.svc.cluster.local"
  }
  set {
    name  = "image.tag"
    value = "1.6.1-amd64"
  }
}
