resource "helm_release" "grafana" {
  name             = "grafana"
  chart            = "grafana"
  timeout          = 600
  repository       = "https://grafana.github.io/helm-charts"
  create_namespace = true
  namespace        = var.monitoring_namespace
  count            = local.monitoring_count

  set {
    name  = "ingress.hosts"
    value = "{grafana.${local.service_name}}"
  }

  set {
    name  = "adminPassword"
    value = "admin"
  }

  set {
    name  = "ingress.enabled"
    value = true
  }

  set {
    name  = "service.type"
    value = "NodePort"
  }
  set {
    name  = "service.port"
    value = "3000"
  }

  set {
    name  = "ingress.annotations.kubernetes\\.io/ingress\\.class"
    value = "nginx"

  }





  values = [<<EOF
ingress:
    enabled: true
#    annotations: {
#      kubernetes.io/ingress.class: nginx
#    }
    path: /


datasources: 
  datasources.yaml:
    apiVersion: 1
    datasources:
    - name: Prometheus
      type: prometheus
      url: http://prometheus-server
      access: proxy
      isDefault: true
    - name: Loki
      type: loki
      url: http://loki:3100
      access: proxy
      isDefault: false


dashboardProviders: 
  dashboardproviders.yaml:
    apiVersion: 1
    providers:
    - name: 'default'
      orgId: 1
      folder: ''
      type: file
      disableDeletion: false
      editable: true
      options:
        path: /var/lib/grafana/dashboards/default

    - name: 'prometheus'
      orgId: 1
      folder: 'Spring Cloud Task'
      type: file
      disableDeletion: true
      editable: true
      options:
        path: /var/lib/grafana/dashboards/prometheus


dashboards: 
  default:
  prometheus:
    spring-cloud-task:
      gnetId: 11436
      datasource: Prometheus
    k8s:
      gnetId: 747
      datasource: Prometheus



EOF
  ]


}
