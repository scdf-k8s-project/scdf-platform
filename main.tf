terraform {
  required_version = ">= 0.12.29"
}


provider "kubernetes-alpha" {
  config_path = "~/.kube/config"
  version     = "~> 0.2"
}

provider "helm" {
  version = "~> 1.3.0"
}

data "google_compute_address" "ip_address" {
  name = "ip-adress-for-nginx-ingress"

}

locals {
  # service_name = var.is_minikube == true ? "${var.domain_name}" : "${data.google_compute_address.ip_address.address}.xip.io"
  service_name     = var.is_minikube == true ? "${var.domain_name}" : data.google_compute_address.ip_address.address == null ? "${var.domain_name}" : "${data.google_compute_address.ip_address.address}.xip.io"
  monitoring_count = var.is_monitoring_enable ? 1 : 0
}



provider "google" {
  version     = "~> 3.15.0"
  project     = var.project_name
  region      = var.region_name
  credentials = file(var.cred_file)

}



