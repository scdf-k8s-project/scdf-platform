variable "region_name" {
  default = "europe-north1"
   description = "Регион в GCP где производится развертываение"
}
variable "cluster_name" {
  default     = "av-k8s-cluster"
  description = "Имя кластера в котором производится развертывание"
}


variable "data_namespace" {
  type        = string
  default     = "datacloud"
  description = "Неймспейс для развертывания платформы SCDF"
}

variable "monitoring_namespace" {
  type        = string
  default     = "monitoring"
  description = "Неймспейс для развертывания инфраструктуры мониторинга и логирования"
}

variable "is_monitoring_enable" {
  description = "Развертывать ли систему мониторинга и логирования"
  default     = true
}


variable "domain_name" {
  description = "Имя домена для развертывания - по умолчнию local, при развертывании в облаке - опредеяется по IP"
  type        = string
  default     = "local"
}

variable "ingress_namespace" {
  type        = string
  default     = "ingress"
  description = "Неймспейс для оазвертывания Ingress Nginx"
}
variable "is_minikube" {
  default     = false
  description = "Развертывается ли в среде миникуба - необходимо для тестирования инфраструктуры"
}




variable "project_name" {
  type = string
  description = "Код проекта в GCP"
}

variable "cred_file" {
  type = string
  description = "Файл с сервисным акаунтом в GCP"
}
