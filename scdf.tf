resource "helm_release" "scdf" {

  name             = "scdf"
  chart            = "spring-cloud-dataflow"
  namespace        = var.data_namespace
  timeout          = 600
  repository       = "https://charts.bitnami.com/bitnami"
  create_namespace = true


  depends_on = [helm_release.prometheus, kubernetes_manifest.crd_servicemonitors_monitoring_coreos_com]


  set {
    name  = "server.ingress.hostname"
    value = "dataflow.${local.service_name}"

  }


  set {
    name  = "server.ingress.annotations.kubernetes\\.io/ingress\\.class"
    value = "nginx"
  }


  set {
    name  = "server.ingress.enabled"
    value = "true"
  }
  set {
    name  = "metrics.serviceMonitor.enabled"
    value = local.monitoring_count == 1
  }

  set {
    name  = "features.metrics.enabled"
    value = "true"
  }
  set {
    name  = "metrics.enabled"
    value = "true"
  }

  set {
    name  = "metrics.serviceMonitor.namespace"
    value = var.monitoring_namespace
  }

  #  provisioner "local-exec" {
  #    when       = destroy
  #    command    = ["kubectl delete pvc data-scdf-rabbitmq-0 -n ${self.namespace}", 
  #        "kubectl delete pvc data-scdf-mariadb-0 -n ${self.namespace}"]
  #    on_failure = continue
  #  }



}

